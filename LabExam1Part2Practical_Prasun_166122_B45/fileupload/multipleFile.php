<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Multiple File Upload</title>
    <style>

    </style>
</head>
<body>
<?php
if(isset($_POST['submit'])){
    if(count($_FILES['upload']['name']) > 0){
        for($i=0; $i<count($_FILES['upload']['name']); $i++) {
            $temp = $_FILES['upload']['tmp_name'][$i];
            if($temp != ""){
                $fileName = $_FILES['upload']['name'][$i];
                $path = "uploaded/" . date('d-m-Y-H-i-s').'-'.$_FILES['upload']['name'][$i];
                if(move_uploaded_file($temp, $path)) {
                    $files[] = $fileName;

                }
            }
        }
    }
    echo "<h1>Uploaded:</h1>";
    if(is_array($files)){
        echo "<ul>";
        foreach($files as $file){
            echo "<li>$file</li>";
        }
        echo "</ul>";
    }
}
?>

<form action="" enctype="multipart/form-data" method="post">

    <div>
        <label for='upload'>Add Attachments:</label>
        <input id='upload' name="upload[]" type="file" multiple="multiple" />
    </div>

    <p><input type="submit" name="submit" value="Upload File"></p>

</form>
</body>
</html>


